﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4.Models;
using System.Collections.Generic;

namespace IdentityServer_EF
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
                   new IdentityResource[]
                   {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email()
                   };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("scope1"),
                new ApiScope("scope2"),
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                // m2m client credentials flow client
                new Client
                {
                    ClientId = "m2m.client",
                    ClientName = "Client Credentials Client",

                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = { new Secret("511536EF-F270-4058-80CA-1C89C192F69A".Sha256()) },

                    AllowedScopes = { "scope1" }
                },

                new Client
                {
                    ClientId = "Todos_Spa_1",
                    ClientName = "Client Modelo Identity",
                    ProtocolType = "oidc",
                    Description = "Client Modelo para Estudo",
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AllowOfflineAccess = true,
                    AbsoluteRefreshTokenLifetime = 31536000, //365 days
                    AccessTokenLifetime = 31536000, //365 days
                    AuthorizationCodeLifetime = 300,
                    IdentityTokenLifetime = 300,
                    RequireConsent = false,
                    //FrontChannelLogoutUri = "http://localhost:5001/signout-oidc",
                    FrontChannelLogoutUri = null,
                    RequireClientSecret = false,
                    RequirePkce = false,
                    //AllowedScopes = { "openid", "profile", "api1" },
                    AllowedScopes = { "email","openid","profile","role","phone","address", "scope1"},                    
                    //AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,
                    AllowedGrantTypes = new[] { "password", "client_credentials", "authorization_code" },
                    ClientSecrets = { new Secret("1q2w3e*") },
                    RedirectUris = { "http://localhost:3000/api/auth/callback/identity-server4" },
                    PostLogoutRedirectUris = { "http://localhost:3000/" },
                    //AllowedCorsOrigins = new[] { "http://localhost:3000/".RemovePostFix("/") },
                    AllowedCorsOrigins = new[] { "http://localhost:3000" },
                    

                },

                // interactive client using code flow + pkce
                new Client
                {
                    ClientId = "interactive",
                    ClientSecrets = { new Secret("49C1A7E1-0C79-4A89-A3D6-A37998FB86B0".Sha256()) },

                    AllowedGrantTypes = GrantTypes.Code,

                    RedirectUris = { "https://localhost:44300/signin-oidc" },
                    FrontChannelLogoutUri = "https://localhost:44300/signout-oidc",
                    PostLogoutRedirectUris = { "https://localhost:44300/signout-callback-oidc" },

                    AllowOfflineAccess = true,
                    AllowedScopes = { "openid", "profile", "scope2" }
                },
            };
    }
}
