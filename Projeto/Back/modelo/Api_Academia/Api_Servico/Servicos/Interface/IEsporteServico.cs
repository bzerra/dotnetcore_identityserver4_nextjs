﻿using Api_Dominio.Entidades;
using Api_Dominio.Enum;
using Flunt.Notifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Api_Servico.Servicos.Interface
{
    public interface IEsporteServico 
    {
        Task<Esporte> CadastrarEsporteAsync(string Nome, ETipoEsporte Tipo, bool Status);
        Task<Esporte> ModificarEsporteAsync(Guid Id, string Nome, ETipoEsporte Tipo, bool Status);
        Task<bool> DeletarEsporteAsync(Guid Id);
        Task<IEnumerable<Esporte>> Get(Guid? Id = null, string Nome = "", ETipoEsporte? Tipo = null, bool? Status = null);
    }
}
