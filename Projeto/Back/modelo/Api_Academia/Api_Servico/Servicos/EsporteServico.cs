﻿using Api_Dominio.Entidades;
using Api_Dominio.Enum;
using Api_Infra.Contexto.Academia;
using Api_Servico.Servicos.Interface;
using Flunt.Notifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api_Servico.Servicos
{
    public class EsporteServico : IEsporteServico
    {
        private readonly AcademiaContext _context;

        public EsporteServico(AcademiaContext context)
        {
            _context = context;
        }        

        public Task<Esporte> CadastrarEsporteAsync(string Nome, ETipoEsporte Tipo, bool Status)
        {
            var esporte = new Esporte(Nome,Tipo, Status);

            _context.Add(esporte);
            _context.SaveChanges();

            return Task.FromResult(esporte);
        }

        public Task<bool> DeletarEsporteAsync(Guid Id)
        {
            var esporte = _context.Esporte.Find(Id);

            _context.Remove(esporte);
            _context.SaveChanges();

            return Task.FromResult(true);
        }

        public async Task<IEnumerable<Esporte>> Get(Guid? Id = null, string Nome = "", ETipoEsporte? Tipo = null, bool? Status = null)
        {
            var listEsporte = await _context.Esporte.ToListAsync() ;

            if (listEsporte.Any())
            {
                if (Id.HasValue)
                    listEsporte = listEsporte.Where(x => x.Id == Id.Value).ToList();

                if (!string.IsNullOrEmpty(Nome))
                    listEsporte = listEsporte.Where(x => x.Nome.Contains(Nome)).ToList();

                if (Tipo.HasValue)
                    listEsporte = listEsporte.Where(x => x.Tipo == Tipo.Value).ToList();

                if (Status.HasValue)
                    listEsporte = listEsporte.Where(x => x.Status == Status.Value).ToList();
            }

            return listEsporte.AsEnumerable();
        }

        public Task<Esporte> ModificarEsporteAsync(Guid Id, string Nome, ETipoEsporte Tipo, bool Status)
        {
            var esporte = _context.Esporte.Find(Id);

            esporte.ModificarNome(Nome).ModificarTipo(Tipo).ModificarStatus(Status);

            _context.Update(esporte);
            _context.SaveChanges();

            return Task.FromResult(esporte);
        }
    }
}
