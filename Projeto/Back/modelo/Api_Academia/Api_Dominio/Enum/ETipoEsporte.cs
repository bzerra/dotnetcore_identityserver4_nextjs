﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api_Dominio.Enum
{
    public enum ETipoEsporte
    {
        COLETIVO = 0,
        INDIVIDUAL = 1
    }
}
