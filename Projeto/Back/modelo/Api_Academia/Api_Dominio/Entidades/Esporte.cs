﻿using Api_Dominio.Enum;
using Flunt.Notifications;
using Flunt.Validations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api_Dominio.Entidades
{
    public class Esporte : Notifiable
    {
        #region Construtor        

        public Esporte(string nome, ETipoEsporte tipo, bool status)
        {
            Id = Guid.NewGuid();
            ModificarNome(nome, novo: true);            
            ModificarTipo(tipo, novo: true);            
            ModificarStatus(status, novo: true);            
        }

        
        #endregion

        #region Propridades
        public Guid Id { get; private set; }
        public string Nome { get; private set; }
        public ETipoEsporte Tipo { get; private set; }
        public bool Status { get; private set; }
        #endregion

        #region Metodos
        public Esporte ModificarStatus(bool status, bool novo = false)
        {
            if (!novo && Status.Equals(status))
                return this;

            Status = status;

            return this;
        }

        public Esporte ModificarTipo(ETipoEsporte tipo, bool novo = false)
        {
            if (!novo && Tipo.Equals(tipo))
                return this;

            Tipo = tipo;            

            return this;
        }

        public Esporte ModificarNome(string nome, bool novo = false)
        {
            if (!novo && Nome.Equals(nome))
                return this;

            Nome = nome;
            new Contract()
                .IsNullOrEmpty(Nome, "Name", "Valor obrigatório");

            return this;
        }
        #endregion
    }
}
