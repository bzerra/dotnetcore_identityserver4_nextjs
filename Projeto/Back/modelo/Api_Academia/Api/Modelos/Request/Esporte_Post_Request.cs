﻿using Api_Dominio.Enum;
using System.ComponentModel.DataAnnotations;

namespace Api.Modelos.Request
{
    public class Esporte_Post_Request
    {
        [Required]
        public string Nome { get; set; }
        [Required]
        public ETipoEsporte Tipo { get; set; }
        [Required]
        public bool Status { get; set; }
    }
}
