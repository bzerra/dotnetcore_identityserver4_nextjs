﻿using Api_Dominio.Enum;
using System;

namespace Api.Modelos.Request
{
    public class Esporte_GetAll_Request
    {
        public Guid? Id { get; set; }
        public string Nome { get; set; }
        public ETipoEsporte? Tipo { get; set; }
        public bool? Status { get; set; }
    }
}
