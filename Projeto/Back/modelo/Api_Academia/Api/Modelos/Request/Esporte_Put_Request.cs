﻿using Api_Dominio.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace Api.Modelos.Request
{
    public class Esporte_Put_Request
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public ETipoEsporte Tipo { get; set; }
        [Required]
        public bool Status { get; set; }
    }
}
