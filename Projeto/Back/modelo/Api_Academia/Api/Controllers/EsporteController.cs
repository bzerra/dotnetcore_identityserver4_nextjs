﻿using Api.Modelos.Request;
using Api_Servico.Servicos.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace modelo_API.Controllers
{
    [ApiController]
    [Route("[controller]"), Authorize]    
    public class EsporteController : Controller
    {
        private IEsporteServico _serviceEsporte { get; set; }

        public EsporteController(IEsporteServico serviceEsporte)
        {
            _serviceEsporte = serviceEsporte;
        }


        #region POST
        [HttpPost]
        public async Task<IActionResult> Post(Esporte_Post_Request request)
        {
            var result = await _serviceEsporte.CadastrarEsporteAsync(request.Nome, request.Tipo, request.Status);

            return Ok(result);
        }
        #endregion

        #region GET
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] Esporte_GetAll_Request request)
        {
            var result = await _serviceEsporte.Get(request.Id, request.Nome, request.Tipo, request.Status);

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] Guid id)
        {
            var result = await _serviceEsporte.Get(Id: id);
            if (result.Any())
                return Ok(result.First());
            else
                return NotFound();               
        }

        #endregion

        #region PUT
        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromRoute] Guid id, [FromBody] Esporte_Put_Request request)
        {
            var result = await _serviceEsporte.ModificarEsporteAsync(id, request.Nome, request.Tipo, request.Status);

            return Ok(result);
        }
        #endregion

        #region DELETE
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            var result = await _serviceEsporte.DeletarEsporteAsync(Id: id);
            if (result)
                return Ok(result);
            else
                return NotFound();                        
        }
        #endregion
    }
}
