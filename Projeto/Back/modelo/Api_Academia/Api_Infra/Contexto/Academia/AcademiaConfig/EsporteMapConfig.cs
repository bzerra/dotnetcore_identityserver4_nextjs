﻿using Api_Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api_Infra.Contexto.Academia.AcademiaConfig
{
    public class EsporteMapConfig : IEntityTypeConfiguration<Esporte>
    {
        public void Configure(EntityTypeBuilder<Esporte> builder)
        {
            builder.ToTable("Esporte");

            builder.HasKey(t => t.Id);
            builder.Property(x => x.Id).HasColumnName("Id").IsRequired();
            builder.Property(x => x.Nome).HasColumnName("Nome").HasMaxLength(50).IsRequired();
            builder.Property(x => x.Tipo).HasColumnName("Tipo").HasMaxLength(50).IsRequired();
            builder.Property(x => x.Status).HasColumnName("Status").IsRequired();
        }
    }
}
